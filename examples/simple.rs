extern crate gslmatrix;

use gslmatrix::gsl;

fn main() {
    let v1 = vec![
        vec![2.15, 1.10, 5.15],
        vec![1.25, 2.35, 7.55],
        vec![2.44, 3.55, 8.25],
    ];

    let v2 = vec![
        vec![10.21, 9.22],
        vec![12.15, 7.22],
        vec![1.25, 3.25],
    ];

    let mut m1 = gsl::Matrix::new(3, 3);
    let mut m2 = gsl::Matrix::new(3, 2);

    for i in 0..v1.len() {
        for j in 0..v1[0].len() {
            m1.set(i, j, v1[i][j]);
        }
    }

    for i in 0..v2.len() {
        for j in 0..v2[0].len() {
            m2.set(i, j, v2[i][j]);
        }
    }

    let m3 = gsl::Matrix::dot(&m1, &m2);

    println!("{}", m1);
    println!("{}", m2);
    println!("{}", m3);
}