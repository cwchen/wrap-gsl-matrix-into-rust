pub mod gsl {
    use std::ops::Drop;
    use std::fmt;

    #[repr(C)]
    enum CBLAS_TRANSPOSE {
        CblasNoTrans=111,
        CblasTrans=112,
        CblasConjTrans=113
    }

    #[repr(C)]
    struct CBlock {
        size: usize,
        data: *mut f64,
    }

    /*
    impl Copy for CBlock {}

    impl Clone for CBlock {
        fn clone(&self) -> CBlock {
            *self
        }
    }
     */

    #[repr(C)]
    pub struct CMatrix {
        size1: usize,
        size2: usize,
        tda: usize,
        data: *mut f64,
        block: *mut CBlock,
        owner: i32,
    }

    /*
    impl Copy for CMatrix {}

    impl Clone for CMatrix {
        fn clone(&self) -> CMatrix {
            *self
        }
    }
     */

    #[link(name = "gsl")]
    extern {
        fn gsl_matrix_calloc(row: usize, col: usize) -> *mut CMatrix;
        fn gsl_matrix_free(m: *mut CMatrix);
        fn gsl_matrix_get(m: *const CMatrix, i: usize, j: usize) -> f64;
        fn gsl_matrix_set(m: *mut CMatrix, i: usize, j: usize, x: f64);
        fn gsl_blas_dgemm(TransA: CBLAS_TRANSPOSE, TransB: CBLAS_TRANSPOSE,
                              alpha: f64, A: *const CMatrix, B: *const CMatrix,
                              beta: f64, C: *mut CMatrix);
    }

    pub struct Matrix {
        m: *mut CMatrix,
    }

    impl Matrix {
        pub fn new(row: usize, col: usize) -> Matrix {
            let m = unsafe { gsl_matrix_calloc(row, col) };
            Matrix{ m: m }
        }
    }

    impl Drop for Matrix {
        fn drop(&mut self) {
            unsafe { gsl_matrix_free((*self).m); };
        }
    }

    impl Matrix {
        pub fn get(&self, i: usize, j: usize) -> f64 {
            unsafe { gsl_matrix_get((*self).m, i, j) }
        }
    }

    impl Matrix {
        pub fn set(&mut self, i: usize, j: usize, x: f64) {
            unsafe { gsl_matrix_set((*self).m, i, j, x); };
        }
    }

    impl Matrix {
        pub fn dot(a: & Matrix, b: & Matrix) -> Matrix {
            let c = Matrix::new(a.row(), b.col());
            unsafe {
                gsl_blas_dgemm(
                    CBLAS_TRANSPOSE::CblasNoTrans,
                    CBLAS_TRANSPOSE::CblasNoTrans,
                    1.0, a.m, b.m, 0.0, c.m
                )
            }
            c
        }
    }

    impl Matrix {
        fn row(& self) -> usize {
            unsafe { (*(*self).m).size1 }
        }
    }

    impl Matrix {
        fn col(& self) -> usize {
            unsafe { (*(*self).m).size2 }
        }
    }

    impl fmt::Display for Matrix {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            let mut output = String::new();
            for i in 0..self.row() {
                for j in 0..self.col() {
                    output += &format!("{}", self.get(i, j));

                    if j < self.col() - 1 {
                        output += ", "
                    }
                }

                output += &format!("\n");
            }
            write!(f, "{}", output)
        }
    }
}
