# Wrap GSL Matrix into Rust

This tiny program demonstrates how to call a C library from Rust.

## System Requirements

* GSL (GNU Scientific Library)
* Rust

## Usage

Clone the repo:

```
$ git clone https://gitlab.com/cwchen/wrap-gsl-matrix-into-rust.git
```

Change the working directory into the root of the cloned repo:

```
$ cd wrap-gsl-matrix-into-rust
```

Compile the program:

```
$ cargo build --release --example simple
```

Run the program:

```
$ ./target/release/examples/simple
```

## Copyright

Licensed as GSL (GNU Scientific Library) itself.
